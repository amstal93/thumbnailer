﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.V1.Controllers;

using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using Hellang.Middleware.ProblemDetails;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using PommaLabs.Thumbnailer.Client.Models.Enumerations;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Services.Managers.Job;

/// <summary>
///   Base controller.
/// </summary>
[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileDetails))]
[ProducesResponseType(StatusCodes.Status400BadRequest)]
[ProducesResponseType(StatusCodes.Status401Unauthorized)]
[ProducesResponseType(StatusCodes.Status403Forbidden)]
[ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
[ProducesResponseType(StatusCodes.Status424FailedDependency)]
public abstract class ThumbnailerControllerBase : ControllerBase
{
    private readonly IJobManager _jobManager;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="jobManager">Job manager,</param>
    protected ThumbnailerControllerBase(IJobManager jobManager)
    {
        _jobManager = jobManager;
    }

    /// <summary>
    ///   Enqueue specified command and waits the job has been processed. This method also handles
    ///   error cases, such as failed jobs.
    /// </summary>
    /// <param name="command">Command.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>Job result.</returns>
    /// <exception cref="ProblemDetailsException">Job has failed.</exception>
    protected async Task<TempFileMetadata> EnqueueAndWaitCommandAsync(ThumbnailerCommandBase command, CancellationToken cancellationToken)
    {
        var job = await _jobManager.EnqueueCommandAsync(command, cancellationToken);

        var jobProcessed = false;
        while (!jobProcessed)
        {
            job = (await _jobManager.QueryJobAsync(job.JobId, cancellationToken))!;

            switch (job.Status)
            {
                case JobStatus.Queued:
                case JobStatus.Processing:
                    break;

                case JobStatus.Processed:
                    jobProcessed = true;
                    break;

                case JobStatus.Failed:
                    throw new ProblemDetailsException(StatusCodes.Status500InternalServerError, job.FailureReason!);

                default:
                    throw new ProblemDetailsException(StatusCodes.Status500InternalServerError, "Async job has an invalid status");
            }

            await Task.Delay(250, cancellationToken);
        }

        return (await _jobManager.GetJobResultAsync(job.JobId, cancellationToken))!;
    }

    /// <summary>
    ///   Produces the response according to the desired type.
    /// </summary>
    /// <param name="responseType">Response type.</param>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="fileNameWithoutExtension">File name without extension.</param>
    /// <param name="tempFile">Temporary file.</param>
    /// <returns>The response, computed according to the desired type.</returns>
    [SuppressMessage("Security", "SCS0018", Justification = "Path does not depend on user input")]
    protected async Task<IActionResult> RespondFileAsync(
        ResponseType responseType, bool openInBrowser,
        string fileNameWithoutExtension, TempFileMetadata tempFile)
    {
        var fileName = fileNameWithoutExtension + tempFile.Extension;

        switch (responseType)
        {
            case ResponseType.Binary:
                // Set up the "Content-Disposition" header with proper encoding of the file name.
                var dispositionType = openInBrowser ? "inline" : "attachment";
                var contentDisposition = new ContentDispositionHeaderValue(dispositionType) { FileName = fileName };
                Response.Headers[HeaderNames.ContentDisposition] = contentDisposition.ToString();

                return PhysicalFile(
                    tempFile.Path,
                    tempFile.ContentType,
                    enableRangeProcessing: true);

            case ResponseType.Base64:
                return Ok(new FileDetails
                {
                    Contents = await System.IO.File.ReadAllBytesAsync(tempFile.Path),
                    ContentType = tempFile.ContentType,
                    FileName = fileName
                });

            default:
                return BadRequest("Invalid response type");
        }
    }
}
