﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.V1.Controllers;

using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Services.Managers.Download;
using PommaLabs.Thumbnailer.Services.Managers.Job;
using PommaLabs.Thumbnailer.Services.Stores.TempFiles;

/// <summary>
///   Controller which handles media file optimization.
/// </summary>
[ApiController, Route("api/v{version:apiVersion}/optimize"), Authorize]
public sealed class OptimizeController : ThumbnailerControllerBase
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="jobManager">Job manager,</param>
    public OptimizeController(IJobManager jobManager) : base(jobManager)
    {
    }

    /// <summary>
    ///   Optimizes given source media file.
    /// </summary>
    /// <param name="downloadManager">Download manager.</param>
    /// <param name="fileUri">Source file URI.</param>
    /// <param name="responseType">Response type.</param>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An optimized version of given source media file.</returns>
    [HttpGet("")]
    public async Task<IActionResult> OptimizeMediaAsync(
        [FromServices] IDownloadManager downloadManager,
        [FromQuery] Uri fileUri,
        [FromQuery] ResponseType responseType = ResponseType.Binary,
        [FromQuery] bool openInBrowser = false,
        CancellationToken cancellationToken = default)
    {
        var localFile = await downloadManager.DownloadFileAsync(fileUri, cancellationToken);
        var moCommand = new MediaOptimizationCommand(localFile);
        var optimized = await EnqueueAndWaitCommandAsync(moCommand, cancellationToken);
        return await RespondFileAsync(responseType, openInBrowser, nameof(optimized), optimized);
    }

    /// <summary>
    ///   Optimizes given source media file.
    /// </summary>
    /// <param name="tempFileStore">Temporary file store.</param>
    /// <param name="file">Source file.</param>
    /// <param name="responseType">Response type.</param>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An optimized version of given source media file.</returns>
    [HttpPost(""), Consumes(Constants.MultipartFormData)]
    [DisableRequestSizeLimit /* Upload limit is globally set in Startup code. */]
    public async Task<IActionResult> OptimizeMediaAsync(
        [FromServices] ITempFileStore tempFileStore,
        IFormFile file,
        [FromQuery] ResponseType responseType = ResponseType.Binary,
        [FromQuery] bool openInBrowser = false,
        CancellationToken cancellationToken = default)
    {
        var localFile = await tempFileStore.HandleFileUploadAsync(file, cancellationToken);
        var moCommand = new MediaOptimizationCommand(localFile);
        var optimized = await EnqueueAndWaitCommandAsync(moCommand, cancellationToken);
        return await RespondFileAsync(responseType, openInBrowser, nameof(optimized), optimized);
    }

    /// <summary>
    ///   Optimizes given source media file.
    /// </summary>
    /// <param name="tempFileStore">Temporary file store.</param>
    /// <param name="file">Source file.</param>
    /// <param name="responseType">Response type.</param>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An optimized version of given source media file.</returns>
    /// <remarks>Source file contents need to be encoded as Base64 string.</remarks>
    [HttpPost("base64"), Consumes(MimeTypeMap.APPLICATION.JSON, MimeTypeMap.APPLICATION.XML)]
    [DisableRequestSizeLimit /* Upload limit is globally set in Startup code. */]
    public async Task<IActionResult> OptimizeMediaAsync(
        [FromServices] ITempFileStore tempFileStore,
        [FromBody] FileDetails file,
        [FromQuery] ResponseType responseType = ResponseType.Binary,
        [FromQuery] bool openInBrowser = false,
        CancellationToken cancellationToken = default)
    {
        var localFile = await tempFileStore.HandleFileUploadAsync(file, cancellationToken);
        var moCommand = new MediaOptimizationCommand(localFile);
        var optimized = await EnqueueAndWaitCommandAsync(moCommand, cancellationToken);
        return await RespondFileAsync(responseType, openInBrowser, nameof(optimized), optimized);
    }
}
