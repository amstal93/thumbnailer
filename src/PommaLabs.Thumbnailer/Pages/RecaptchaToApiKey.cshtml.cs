﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Pages;

using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Services.Stores.ApiKeys;
using reCAPTCHA.AspNetCore;

/// <summary>
///   Converts a valid reCAPTCHA token into a temporary API key.
/// </summary>
[AllowAnonymous]
public sealed class RecaptchaToApiKeyModel : PageModel
{
    private readonly IApiKeyStore _apiKeyStore;
    private readonly ILogger<RecaptchaToApiKeyModel> _logger;
    private readonly IRecaptchaService _recaptchaService;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="recaptchaService">reCAPTCHA service.</param>
    /// <param name="apiKeyStore">API key store.</param>
    /// <param name="logger">Logger.</param>
    public RecaptchaToApiKeyModel(
        IRecaptchaService recaptchaService,
        IApiKeyStore apiKeyStore,
        ILogger<RecaptchaToApiKeyModel> logger)
    {
        _recaptchaService = recaptchaService;
        _apiKeyStore = apiKeyStore;
        _logger = logger;
    }

    /// <summary>
    ///   Converts a valid reCAPTCHA token into a temporary API key.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token.</param>
    public async Task<JsonResult> OnPost(CancellationToken cancellationToken)
    {
        var recaptchaResponse = await _recaptchaService.Validate(Request);

        if (!recaptchaResponse.success)
        {
            _logger.LogWarning("Received an invalid reCAPTCHA token from host '{HostName}', returning an invalid API key", recaptchaResponse.hostname);
            return new JsonResult(ApiKeyCredentials.Invalid);
        }

        _logger.LogInformation("Received a valid reCAPTCHA token from host '{HostName}', returning a valid temporary API key", recaptchaResponse.hostname);
        return new JsonResult(await _apiKeyStore.AddTempApiKeyAsync(cancellationToken));
    }
}
