﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Pages;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.RazorPages;

/// <summary>
///   Home page.
/// </summary>
[AllowAnonymous]
public sealed class IndexModel : PageModel
{
    /// <summary>
    ///   Returns the page.
    /// </summary>
    public void OnGet()
    {
        // This is a static page, no processing needs to be done on the server.
    }
}
