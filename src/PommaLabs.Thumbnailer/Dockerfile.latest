# https://gitlab.com/pommalabs/thumbnailer/-/tree/base-image
FROM registry.gitlab.com/pommalabs/thumbnailer:latest-base AS base
HEALTHCHECK --interval=30s --timeout=5s \
  CMD curl -f http://localhost:${PORT}/health || exit 1

FROM pommalabs/dotnet:6-sdk AS build
COPY ./NuGet.config ./
COPY ./src/PommaLabs.Thumbnailer/PommaLabs.Thumbnailer.csproj ./src/PommaLabs.Thumbnailer/
COPY ./src/PommaLabs.Thumbnailer.Client/PommaLabs.Thumbnailer.Client.csproj ./src/PommaLabs.Thumbnailer.Client/
RUN dotnet restore "src/PommaLabs.Thumbnailer/PommaLabs.Thumbnailer.csproj"
COPY . .
WORKDIR /opt/sln/src/PommaLabs.Thumbnailer
RUN dotnet build PommaLabs.Thumbnailer.csproj -c Release -o /opt/app/build

FROM build AS publish
RUN dotnet publish PommaLabs.Thumbnailer.csproj -c Release -o /opt/app/publish

FROM base AS final
COPY --from=publish /opt/app/publish .
CMD ["dotnet", "PommaLabs.Thumbnailer.dll"]

# Grant full access on Thumbnailer data directory to application user.
USER root
RUN chown -R app:app /opt/app/Data

# Switch back to application user, whose ID is 1000.
USER 1000
