﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.OperationFilters;

using System.Linq;
using Microsoft.OpenApi.Models;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using Swashbuckle.AspNetCore.SwaggerGen;

/// <summary>
///   Marks certain Swagger operation parameters as deprecated.
/// </summary>
public sealed class BinarySchemaOperationFilter : IOperationFilter
{
    /// <inheritdoc/>
    public void Apply(OpenApiOperation operation, OperationFilterContext context)
    {
        var okResponse = operation.Responses["200"];
        if (okResponse.Content["text/plain"]?.Schema?.Reference?.Id == nameof(FileDetails))
        {
            var previousContent = okResponse.Content.ToList();

            okResponse.Content.Clear();
            okResponse.Content.Add("application/octet-stream", new OpenApiMediaType
            {
                Schema = new OpenApiSchema { Type = "string", Format = "binary" }
            });

            foreach (var pc in previousContent)
            {
                okResponse.Content.Add(pc.Key, pc.Value);
            }
        }
    }
}
