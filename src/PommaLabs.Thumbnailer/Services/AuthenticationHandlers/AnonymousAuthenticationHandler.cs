﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.AuthenticationHandlers;

using System.Collections.Generic;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Configurations;

/// <summary>
///   Anonymous authentication handler.
/// </summary>
public sealed class AnonymousAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
{
    private readonly IOptions<SecurityConfiguration> _securityConfiguration;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="options">Options.</param>
    /// <param name="logger">Logger.</param>
    /// <param name="encoder">Encoder.</param>
    /// <param name="clock">Clock.</param>
    /// <param name="securityConfiguration">Security configuration.</param>
    public AnonymousAuthenticationHandler(
        IOptionsMonitor<AuthenticationSchemeOptions> options,
        ILoggerFactory logger,
        UrlEncoder encoder,
        ISystemClock clock,
        IOptions<SecurityConfiguration> securityConfiguration)
        : base(options, logger, encoder, clock)
    {
        _securityConfiguration = securityConfiguration;
    }

    /// <inheritdoc/>
    protected override Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        if (!_securityConfiguration.Value.AllowAnonymousAccess)
        {
            return Task.FromResult(AuthenticateResult.Fail("Anonymous access is not allowed"));
        }

        var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, Constants.AnonymousIdentityName)
            };

        var identity = new ClaimsIdentity(claims, Constants.AnonymousAuthenticationScheme);
        var identities = new List<ClaimsIdentity> { identity };
        var principal = new ClaimsPrincipal(identities);
        var ticket = new AuthenticationTicket(principal, Constants.AnonymousAuthenticationScheme);

        return Task.FromResult(AuthenticateResult.Success(ticket));
    }
}
