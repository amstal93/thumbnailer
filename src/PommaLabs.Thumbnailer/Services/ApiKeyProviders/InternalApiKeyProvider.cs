﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.ApiKeyProviders;

using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AspNetCore.Authentication.ApiKey;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Services.Stores.ApiKeys;

internal sealed class InternalApiKeyProvider : IApiKeyProvider
{
    private readonly IApiKeyStore _apiKeyStore;

    public InternalApiKeyProvider(IApiKeyStore apiKeyStore)
    {
        _apiKeyStore = apiKeyStore;
    }

    async Task<IApiKey?> IApiKeyProvider.ProvideAsync(string key)
    {
        var dto = await _apiKeyStore.ValidateApiKeyAsync(key, default);
        return dto == null ? null : new ApiKey(dto);
    }

    private sealed class ApiKey : IApiKey
    {
        public ApiKey(ApiKeyCredentials dto)
        {
            Key = dto.Value;
            OwnerName = dto.Name;
            Claims = new[] { new Claim(ClaimTypes.Name, dto.Name) };
        }

        public IReadOnlyCollection<Claim> Claims { get; }

        public string Key { get; }

        public string OwnerName { get; }
    }
}
