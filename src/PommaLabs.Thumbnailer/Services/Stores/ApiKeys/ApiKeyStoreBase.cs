﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Stores.ApiKeys;

using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Base class for API key stores.
/// </summary>
public abstract class ApiKeyStoreBase : IApiKeyStore
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="securityConfiguration">Security configuration.</param>
    protected ApiKeyStoreBase(IOptions<SecurityConfiguration> securityConfiguration)
    {
        SecurityConfiguration = securityConfiguration;
    }

    /// <summary>
    ///   Security configuration.
    /// </summary>
    protected IOptions<SecurityConfiguration> SecurityConfiguration { get; }

    /// <inheritdoc/>
    public abstract Task<ApiKeyCredentials> AddTempApiKeyAsync(CancellationToken cancellationToken);

    /// <inheritdoc/>
    public abstract Task<int> DeleteExpiredTempApiKeysAsync(CancellationToken cancellationToken);

    /// <inheritdoc/>
    public abstract Task<ApiKeyCredentials?> ValidateApiKeyAsync(string apiKey, CancellationToken cancellationToken);

    /// <summary>
    ///   Returns true if provided API keys match, false otherwise.
    /// </summary>
    /// <param name="expected">Expected API key.</param>
    /// <param name="received">Received API key.</param>
    /// <returns></returns>
    protected bool ApiKeysMatch(string expected, string received)
    {
        if (!SecurityConfiguration.Value.EnableCaseSensitiveApiKeyValidation)
        {
            expected = expected.ToUpperInvariant();
            received = received.ToUpperInvariant();
        }
        var expectedHash = ComputeSha384Hash(expected);
        var receivedHash = ComputeSha384Hash(received);
        return CryptographicOperations.FixedTimeEquals(expectedHash, receivedHash);
    }

    private static byte[] ComputeSha384Hash(string str)
    {
        using var sha384Hash = SHA384.Create();
        var strBytes = Encoding.UTF8.GetBytes(str);
        return sha384Hash.ComputeHash(strBytes);
    }
}
