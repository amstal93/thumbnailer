﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Job;

using System;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Client.Models.DTO.Public;
using PommaLabs.Thumbnailer.Client.Models.Enumerations;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Job manager which stores concurrent jobs within an in-memory queue and within the distributed
///   cache. Each job is enqueued and executed on a single server, but other servers can respond to
///   job status queries.
/// </summary>
public sealed class ConcurrentJobManager : IJobManager
{
    private readonly ICache _cache;
    private readonly IOptions<DatabaseConfiguration> _databaseConfiguration;
    private readonly IDataProtector _dataProtector;
    private readonly ILogger<ConcurrentJobManager> _logger;
    private readonly BufferBlock<Guid> _queue;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="cache">Cache.</param>
    /// <param name="databaseConfiguration">Database configuration.</param>
    /// <param name="dataProtectionProvider">Data protection provider.</param>
    /// <param name="logger">Logger.</param>
    public ConcurrentJobManager(
        ICache cache,
        IOptions<DatabaseConfiguration> databaseConfiguration,
        IDataProtectionProvider dataProtectionProvider,
        ILogger<ConcurrentJobManager> logger)
    {
        _cache = cache;
        _databaseConfiguration = databaseConfiguration;
        _dataProtector = dataProtectionProvider.CreateProtector(nameof(IJobManager));
        _logger = logger;
        _queue = new BufferBlock<Guid>();
    }

    /// <inheritdoc/>
    public async Task<ThumbnailerCommandBase> DequeueCommandAsync(CancellationToken cancellationToken)
    {
        var internalJobId = await _queue.ReceiveAsync(cancellationToken);
        var jobData = await _cache.GetAsync<JobData>(
            partition: nameof(IJobManager),
            key: new CacheKey(internalJobId),
            cancellationToken: cancellationToken);

        if (!jobData.HasValue)
        {
            _logger.LogWarning("A command with internal job ID '{InternalJobId}' should have been in the cache but it has not been found", internalJobId);
            throw new InvalidOperationException($"Command with internal job ID '{internalJobId}' was not found");
        }

        var (job, command, _) = jobData.Value;

        // Change job status, since it will be processed soon.
        job.Status = JobStatus.Processing;

        await _cache.AddSlidingAsync(
            partition: nameof(IJobManager),
            key: new CacheKey(internalJobId),
            value: new JobData(job, command, null),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);

        return command;
    }

    /// <inheritdoc/>
    public async Task<JobDetails> EnqueueCommandAsync(ThumbnailerCommandBase command, CancellationToken cancellationToken)
    {
        var internalJobId = Guid.NewGuid();
        command.InternalJobId = internalJobId;
        var job = new JobDetails(InternalJobIdToPublic(internalJobId));

        // First step: add job and command to the distributed cache, in order to persist this
        // information and to make it visible from all nodes.
        await _cache.AddSlidingAsync(
            partition: nameof(IJobManager),
            key: new CacheKey(internalJobId),
            value: new JobData(job, command, null),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);

        // Second step: enqueue job ID. Consumers will get job ID and will retrieve job and command
        // information from the distributed cache.
        _queue.Post(internalJobId);

        return job;
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata?> GetJobResultAsync(string publicJobId, CancellationToken cancellationToken)
    {
        try
        {
            var internalJobId = PublicJobIdToInternal(publicJobId);
            var jobData = (await _cache.GetAsync<JobData>(
                partition: nameof(IJobManager),
                key: new CacheKey(internalJobId),
                cancellationToken: cancellationToken)).ValueOrDefault();

            if (jobData == null || jobData.Job.Status != JobStatus.Processed)
            {
                return null;
            }

            return jobData.Result;
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, "An error occurred while getting the result of job with public ID '{PublicJobId}'", publicJobId);
            return null;
        }
    }

    /// <inheritdoc/>
    public async Task MarkJobAsFailedAsync(Guid internalJobId, string failureReason, CancellationToken cancellationToken)
    {
        var jobData = await _cache.GetAsync<JobData>(
            partition: nameof(IJobManager),
            key: new CacheKey(internalJobId),
            cancellationToken: cancellationToken);

        if (!jobData.HasValue)
        {
            _logger.LogWarning("Cannot mark as failed job with internal ID '{InternalJobId}' because it was not found in the cache", internalJobId);
            return;
        }

        var (job, command, _) = jobData.Value;

        if (job.Status != JobStatus.Processing)
        {
            _logger.LogWarning("Cannot mark as failed job with internal ID '{InternalJobId}' because its status is '{JobStatus}'", internalJobId, job.Status);
            return;
        }

        job.Status = JobStatus.Failed;
        job.FailureReason = failureReason;

        await _cache.AddSlidingAsync(
            partition: nameof(IJobManager),
            key: new CacheKey(internalJobId),
            value: new JobData(job, command, null),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);
    }

    /// <inheritdoc/>
    public async Task MarkJobAsProcessedAsync(Guid internalJobId, TempFileMetadata result, CancellationToken cancellationToken)
    {
        var jobData = await _cache.GetAsync<JobData>(
            partition: nameof(IJobManager),
            key: new CacheKey(internalJobId),
            cancellationToken: cancellationToken);

        if (!jobData.HasValue)
        {
            _logger.LogWarning("Cannot mark as processed job with internal ID '{InternalJobId}' because it was not found in the cache", internalJobId);
            return;
        }

        var (job, command, _) = jobData.Value;

        if (job.Status != JobStatus.Processing)
        {
            _logger.LogWarning("Cannot mark as processed job with internal ID '{InternalJobId}' because its status is '{JobStatus}'", internalJobId, job.Status);
            return;
        }

        job.Status = JobStatus.Processed;

        await _cache.AddSlidingAsync(
            partition: nameof(IJobManager),
            key: new CacheKey(internalJobId),
            value: new JobData(job, command, result),
            interval: _databaseConfiguration.Value.CacheLifetime,
            cancellationToken: cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<JobDetails?> QueryJobAsync(string publicJobId, CancellationToken cancellationToken)
    {
        try
        {
            var internalJobId = PublicJobIdToInternal(publicJobId);
            var jobData = await _cache.GetAsync<JobData>(
                partition: nameof(IJobManager),
                key: new CacheKey(internalJobId),
                cancellationToken: cancellationToken);

            return jobData.ValueOrDefault()?.Job;
        }
        catch (Exception ex)
        {
            _logger.LogWarning(ex, "An error occurred while querying command with public job ID '{PublicJobId}'", publicJobId);
            return null;
        }
    }

    private string InternalJobIdToPublic(Guid internalJobId)
    {
        return WebEncoders.Base64UrlEncode(_dataProtector.Protect(internalJobId.ToByteArray()));
    }

    private Guid PublicJobIdToInternal(string publicJobId)
    {
        return new Guid(_dataProtector.Unprotect(WebEncoders.Base64UrlDecode(publicJobId)));
    }

    private sealed record JobData(JobDetails Job, ThumbnailerCommandBase Command, TempFileMetadata? Result);
}
