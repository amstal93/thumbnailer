﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Job;

using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Client.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Job manager which applies validations.
/// </summary>
public sealed class ValidatingJobManager : IJobManager
{
    private readonly IJobManager _jobManager;
    private readonly Validator _validator;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="jobManager">Job manager.</param>
    /// <param name="logger">Logger.</param>
    public ValidatingJobManager(
        IJobManager jobManager,
        ILogger<ValidatingJobManager> logger)
    {
        _jobManager = jobManager;
        _validator = new Validator(logger);
    }

    /// <inheritdoc/>
    public Task<ThumbnailerCommandBase> DequeueCommandAsync(CancellationToken cancellationToken)
    {
        return _jobManager.DequeueCommandAsync(cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<JobDetails> EnqueueCommandAsync(ThumbnailerCommandBase command, CancellationToken cancellationToken)
    {
        // Preconditions
        command.Validate(_validator);

        return await _jobManager.EnqueueCommandAsync(command, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata?> GetJobResultAsync(string publicJobId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(publicJobId)) return null;
        var result = await _jobManager.GetJobResultAsync(publicJobId, cancellationToken);

        // Postconditions
        Trace.Assert(result == null || File.Exists(result.Path), "Job result does not exist on file system");
        return result;
    }

    /// <inheritdoc/>
    public Task MarkJobAsFailedAsync(Guid internalJobId, string failureReason, CancellationToken cancellationToken)
    {
        return _jobManager.MarkJobAsFailedAsync(internalJobId, failureReason, cancellationToken);
    }

    /// <inheritdoc/>
    public Task MarkJobAsProcessedAsync(Guid internalJobId, TempFileMetadata result, CancellationToken cancellationToken)
    {
        return _jobManager.MarkJobAsProcessedAsync(internalJobId, result, cancellationToken);
    }

    /// <inheritdoc/>
    public async Task<JobDetails?> QueryJobAsync(string publicJobId, CancellationToken cancellationToken)
    {
        if (string.IsNullOrWhiteSpace(publicJobId)) return null;
        return await _jobManager.QueryJobAsync(publicJobId, cancellationToken);
    }
}
