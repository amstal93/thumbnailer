﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Download;

using System;
using System.Threading;
using System.Threading.Tasks;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   File download manager.
/// </summary>
public interface IDownloadManager
{
    /// <summary>
    ///   Downloads file from specified URI.
    /// </summary>
    /// <param name="fileUri">File URI.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An internal structure representing the downloaded file.</returns>
    Task<TempFileMetadata> DownloadFileAsync(Uri fileUri, CancellationToken cancellationToken);
}
