﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Download;

using System;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Core;
using PommaLabs.Thumbnailer.Models.Configurations;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Services.Stores.TempFiles;

/// <summary>
///   Download manager which relies on the HTTP protocol.
/// </summary>
public sealed class HttpDownloadManager : IDownloadManager, IDisposable
{
    private readonly HttpClient _httpClient;
    private readonly ILogger<HttpDownloadManager> _logger;
    private readonly ITempFileStore _tempFileStore;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="logger">Logger.</param>
    /// <param name="tempFileStore">Temporary file store.</param>
    /// <param name="securityConfiguration">Security configuration.</param>
    public HttpDownloadManager(
        ILogger<HttpDownloadManager> logger,
        ITempFileStore tempFileStore,
        IOptions<SecurityConfiguration> securityConfiguration)
    {
        _logger = logger;
        _tempFileStore = tempFileStore;

        _httpClient = new HttpClient
        {
            MaxResponseContentBufferSize = securityConfiguration.Value.MaxFileDownloadSizeInBytes,
            Timeout = securityConfiguration.Value.FileDownloadTimeout
        };
    }

    /// <inheritdoc/>
    public void Dispose()
    {
        _httpClient.Dispose();
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> DownloadFileAsync(Uri fileUri, CancellationToken cancellationToken)
    {
        _logger.LogDebug("Downloading file '{FileUri}'", fileUri);
        using var response = await _httpClient.GetAsync(fileUri, cancellationToken);

        if (!response.IsSuccessStatusCode)
        {
            _logger.LogError("An error occurred while downloading file '{FileUri}'. Remote response status code was {ResponseStatusCode}", fileUri, response.StatusCode);
            throw new InvalidOperationException($"File download failed, remote response status code was {response.StatusCode}");
        }

        var contentType = response.Content.Headers.ContentType?.MediaType ?? MimeTypeMap.APPLICATION.OCTET_STREAM;
        var fileName = Helpers.CleanupFileName(response.Content.Headers.ContentDisposition?.FileName);
        var extension = Path.GetExtension(fileName);
        _logger.LogInformation("Handling download of file with content type '{ContentType}', extension '{Extension}' and file name '{FileName}'", contentType, extension, fileName);

        var file = await _tempFileStore.GetTempFileAsync(contentType, extension, cancellationToken);
        if (file.ContentType != contentType)
        {
            _logger.LogInformation("Content type of downloaded file has been overridden with '{ContentType}'", file.ContentType);
        }

        using (var fileStream = File.OpenWrite(file.Path))
        {
            using var responseStream = await response.Content.ReadAsStreamAsync(cancellationToken);
            await responseStream.CopyToAsync(fileStream, cancellationToken);
        }
        _logger.LogDebug("File '{FileUri}' downloaded successfully, it has been stored at path '{FilePath}'", fileUri, file.Path);

        return await _tempFileStore.HandleFileDownloadAsync(file, cancellationToken);
    }
}
