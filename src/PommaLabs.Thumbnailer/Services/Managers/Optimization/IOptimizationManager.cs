﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Optimization;

using System.Threading;
using System.Threading.Tasks;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Media file optimization manager.
/// </summary>
public interface IOptimizationManager
{
    /// <summary>
    ///   Optimizes given media file.
    /// </summary>
    /// <param name="command">Media optimization command.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An optimized version of given media file.</returns>
    Task<TempFileMetadata> OptimizeMediaAsync(MediaOptimizationCommand command, CancellationToken cancellationToken);
}
