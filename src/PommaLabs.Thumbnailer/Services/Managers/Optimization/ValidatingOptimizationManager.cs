﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Optimization;

using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Optimization manager which applies validations.
/// </summary>
public sealed class ValidatingOptimizationManager : IOptimizationManager
{
    private readonly IOptimizationManager _optimizationManager;
    private readonly Validator _validator;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="optimizationManager">Optimization manager.</param>
    /// <param name="logger">Logger.</param>
    public ValidatingOptimizationManager(
        IOptimizationManager optimizationManager,
        ILogger<ValidatingOptimizationManager> logger)
    {
        _optimizationManager = optimizationManager;
        _validator = new Validator(logger);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> OptimizeMediaAsync(MediaOptimizationCommand command, CancellationToken cancellationToken)
    {
        // Preconditions
        command.Validate(_validator);

        var result = await _optimizationManager.OptimizeMediaAsync(command, cancellationToken);

        // Postconditions
        Trace.Assert(File.Exists(result.Path), "Optimized media does not exist on file system");
        return result;
    }
}
