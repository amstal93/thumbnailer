﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.Managers.Thumbnail;

using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Models.Commands;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Thumbnail manager which applies validations.
/// </summary>
public sealed class ValidatingThumbnailManager : IThumbnailManager
{
    private readonly IThumbnailManager _thumbnailManager;
    private readonly Validator _validator;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="thumbnailManager">Thumbnail manager.</param>
    /// <param name="logger">Logger.</param>
    public ValidatingThumbnailManager(
        IThumbnailManager thumbnailManager,
        ILogger<ValidatingThumbnailManager> logger)
    {
        _thumbnailManager = thumbnailManager;
        _validator = new Validator(logger);
    }

    /// <inheritdoc/>
    public async Task<TempFileMetadata> GenerateThumbnailAsync(ThumbnailGenerationCommand command, CancellationToken cancellationToken)
    {
        // Preconditions
        command.Validate(_validator);

        var result = await _thumbnailManager.GenerateThumbnailAsync(command, cancellationToken);

        // Postconditions
        Trace.Assert(File.Exists(result.Path), "Generated thumbnail does not exist on file system");
        return result;
    }
}
