﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Services.HealthChecks;

using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using PommaLabs.Thumbnailer.Services.Stores.TempFiles;

/// <summary>
///   Checks the status of the temporary file store.
/// </summary>
public sealed class TempFileStoreHealthCheck : IHealthCheck
{
    private const int FreeSpaceThreshold = 1 * 1024 * 1024; // 1 MB

    private readonly ITempFileStore _tempFileStore;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="tempFileStore">Temporary file store.</param>
    public TempFileStoreHealthCheck(ITempFileStore tempFileStore)
    {
        _tempFileStore = tempFileStore;
    }

    /// <inheritdoc/>
    public async Task<HealthCheckResult> CheckHealthAsync(
        HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        try
        {
            var freeSpace = await _tempFileStore.GetAvailableFreeSpaceInBytes(cancellationToken);
            if (freeSpace < FreeSpaceThreshold)
            {
                return HealthCheckResult.Unhealthy($"Available free space ({freeSpace} bytes) is below the threshold ({FreeSpaceThreshold} bytes)");
            }
            return HealthCheckResult.Healthy($"Temporary file store has more than {FreeSpaceThreshold} bytes of free space");
        }
        catch (Exception ex)
        {
            return HealthCheckResult.Unhealthy("Temporary file store threw an exception while getting available free space", exception: ex);
        }
    }
}
