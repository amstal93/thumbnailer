﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.Commands;

using System;
using PommaLabs.KVLite;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Represents shared information between Thumbnailer commands.
/// </summary>
public abstract class ThumbnailerCommandBase : IObjectWithCacheKey
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="file">File.</param>
    protected ThumbnailerCommandBase(TempFileMetadata file)
    {
        File = file;
    }

    /// <summary>
    ///   File on which the command should be applied.
    /// </summary>
    public TempFileMetadata File { get; set; }

    /// <summary>
    ///   Internal job ID, available when command is part of an asynchronous job.
    /// </summary>
    public Guid? InternalJobId { get; set; }

    /// <summary>
    ///   ID of the processor which is currently handling this job.
    /// </summary>
    public int ProcessorId { get; set; }

    /// <inheritdoc/>
    public virtual CacheKey GetCacheKey()
    {
        return File.GetCacheKey();
    }

    /// <summary>
    ///   Validates command properties and throws an exception if they are not valid.
    /// </summary>
    /// <param name="validator">Validator.</param>
    internal abstract void Validate(Validator validator);
}
