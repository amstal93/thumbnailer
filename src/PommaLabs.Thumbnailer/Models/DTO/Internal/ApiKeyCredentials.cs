﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Models.DTO.Internal;

/// <summary>
///   Represents an API key.
/// </summary>
public sealed class ApiKeyCredentials
{
    /// <summary>
    ///   An invalid API key.
    /// </summary>
    public static ApiKeyCredentials Invalid { get; } = new ApiKeyCredentials { IsValid = false };

    /// <summary>
    ///   True if API key is valid, false otherwise.
    /// </summary>
    public bool IsValid { get; set; }

    /// <summary>
    ///   Descriptive name.
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    ///   API key value.
    /// </summary>
    public string Value { get; set; } = string.Empty;
}
