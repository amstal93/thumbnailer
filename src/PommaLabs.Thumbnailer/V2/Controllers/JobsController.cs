﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.V2.Controllers;

using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using PommaLabs.Thumbnailer.Client.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.DTO.Internal;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using PommaLabs.Thumbnailer.Models.Enumerations;
using PommaLabs.Thumbnailer.Services.Managers.Job;

/// <summary>
///   Controller which exposes information about queued jobs.
/// </summary>
[ApiController, Route("api/v{version:apiVersion}/jobs/{jobId}"), Authorize]
[ProducesResponseType(StatusCodes.Status404NotFound)]
public sealed class JobsController : ControllerBase
{
    private readonly IJobManager _jobManager;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="jobManager">Job manager.</param>
    public JobsController(IJobManager jobManager)
    {
        _jobManager = jobManager;
    }

    /// <summary>
    ///   Looks for the result of the job with specified ID.
    /// </summary>
    /// <param name="jobId">Job ID.</param>
    /// <param name="responseType">Response type.</param>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The result of the job, if it has been found.</returns>
    [HttpGet("download")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FileDetails))]
    public async Task<IActionResult> DownloadJobResultAsync(
        [FromRoute] string jobId,
        [FromQuery] ResponseType responseType = ResponseType.Binary,
        [FromQuery] bool openInBrowser = false,
        CancellationToken cancellationToken = default)
    {
        var result = await _jobManager.GetJobResultAsync(jobId, cancellationToken);
        if (result == null) return NotFound();
        return await RespondFileAsync(responseType, openInBrowser, nameof(result), result);
    }

    /// <summary>
    ///   Looks for a job with specified ID.
    /// </summary>
    /// <param name="jobId">Job ID.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>The job, if it has been found.</returns>
    [HttpGet("")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(JobDetails))]
    public async Task<IActionResult> QueryJobAsync(string jobId, CancellationToken cancellationToken)
    {
        var job = await _jobManager.QueryJobAsync(jobId, cancellationToken);
        return (job == null) ? NotFound() : Ok(job);
    }

    /// <summary>
    ///   Produces the response according to the desired type.
    /// </summary>
    /// <param name="responseType">Response type.</param>
    /// <param name="openInBrowser">
    ///   When true, browser will receive a "Content-Disposition: inline" header, which should make
    ///   it open the result in-browser. When false, browser will receive a "Content-Disposition:
    ///   attachment" header, which should force it to download the result.
    /// </param>
    /// <param name="fileNameWithoutExtension">File name without extension.</param>
    /// <param name="tempFile">Temporary file.</param>
    /// <returns>The response, computed according to the desired type.</returns>
    [SuppressMessage("Security", "SCS0018", Justification = "Path does not depend on user input")]
    private async Task<IActionResult> RespondFileAsync(
        ResponseType responseType, bool openInBrowser,
        string fileNameWithoutExtension, TempFileMetadata tempFile)
    {
        var fileName = fileNameWithoutExtension + tempFile.Extension;

        switch (responseType)
        {
            case ResponseType.Binary:
                // Set up the "Content-Disposition" header with proper encoding of the file name.
                var dispositionType = openInBrowser ? "inline" : "attachment";
                var contentDisposition = new ContentDispositionHeaderValue(dispositionType) { FileName = fileName };
                Response.Headers[HeaderNames.ContentDisposition] = contentDisposition.ToString();

                return PhysicalFile(
                    tempFile.Path,
                    tempFile.ContentType,
                    enableRangeProcessing: true);

            case ResponseType.Base64:
                return Ok(new FileDetails
                {
                    Contents = await System.IO.File.ReadAllBytesAsync(tempFile.Path),
                    ContentType = tempFile.ContentType,
                    FileName = fileName
                });

            default:
                return BadRequest("Invalid response type");
        }
    }
}
