﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Client.Resources;

internal static class ActivityNames
{
    public const string HealthCheck = "health_check";
    public const string JobRetrieval = "job_retrieval";
    public const string JobResultDownload = "job_result_download";
    public const string MediaOptimization = "media_optimization";
    public const string ThumbnailGeneration = "thumbnail_generation";
}
