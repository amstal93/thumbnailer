﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("PommaLabs.Thumbnailer")]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("PommaLabs.Thumbnailer.IntegrationTests")]

namespace PommaLabs.Thumbnailer.Client;

using System;
using System.Threading;
using System.Threading.Tasks;
using PommaLabs.Thumbnailer.Client.Core;

/// <summary>
///   Thumbnailer client.
/// </summary>
public interface IThumbnailerClient
{
    /// <summary>
    ///   Produces a thumbnail of given source file.
    /// </summary>
    /// <param name="fileUri">Source file URI.</param>
    /// <param name="widthPx">
    ///   Max thumbnail width. Thumbnail will be generated preserving the source file aspect
    ///   ratio and, if <paramref name="fill"/> is true, a transparent background will be added
    ///   so that thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="heightPx">
    ///   Max thumbnail height. Thumbnail will be generated preserving the source file aspect
    ///   ratio and, if <paramref name="fill"/> is true, a transparent background will be added
    ///   so that thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="shavePx">Shaves <paramref name="shavePx"/> from source file edges.</param>
    /// <param name="fill">
    ///   If true, a transparent background will be added so that thumbnail width and height are
    ///   exactly as requested, preserving source file aspect ratio. When
    ///   <paramref name="smartCrop"/> is enabled, this parameter will be ignored. Defaults to true.
    /// </param>
    /// <param name="smartCrop">
    ///   Finds a good crop which satisfies specified <paramref name="widthPx"/> and
    ///   <paramref name="heightPx"/> parameters. When smart crop is enabled,
    ///   <paramref name="fill"/> is ignored. Defaults to false.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail of given source file.</returns>
    Task<byte[]> GenerateThumbnailAsync(
        Uri fileUri,
        ushort widthPx = Validator.ThumbnailSidePx,
        ushort heightPx = Validator.ThumbnailSidePx,
        ushort shavePx = Validator.ThumbnailShavePx,
        bool fill = true,
        bool smartCrop = false,
        CancellationToken cancellationToken = default);

    /// <summary>
    ///   Produces a thumbnail of given source file.
    /// </summary>
    /// <param name="contents">File bytes.</param>
    /// <param name="contentType">
    ///   File content type. If not specified, it will be deduced from file signature: however,
    ///   please specify it if available.
    /// </param>
    /// <param name="fileName">
    ///   File name. If not specified, its value will be a random file name compatible with
    ///   specified (or deduced) content type.
    /// </param>
    /// <param name="widthPx">
    ///   Max thumbnail width. Thumbnail will be generated preserving the source file aspect
    ///   ratio and, if <paramref name="fill"/> is true, a transparent background will be added
    ///   so that thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="heightPx">
    ///   Max thumbnail height. Thumbnail will be generated preserving the source file aspect
    ///   ratio and, if <paramref name="fill"/> is true, a transparent background will be added
    ///   so that thumbnail width and height are exactly as requested.
    /// </param>
    /// <param name="shavePx">Shaves <paramref name="shavePx"/> from source file edges.</param>
    /// <param name="fill">
    ///   If true, a transparent background will be added so that thumbnail width and height are
    ///   exactly as requested, preserving source file aspect ratio. When
    ///   <paramref name="smartCrop"/> is enabled, this parameter will be ignored. Defaults to true.
    /// </param>
    /// <param name="smartCrop">
    ///   Finds a good crop which satisfies specified <paramref name="widthPx"/> and
    ///   <paramref name="heightPx"/> parameters. When smart crop is enabled,
    ///   <paramref name="fill"/> is ignored. Defaults to false.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>A thumbnail of given source file.</returns>
    Task<byte[]> GenerateThumbnailAsync(
        byte[] contents,
        string? contentType = default,
        string? fileName = default,
        ushort widthPx = Validator.ThumbnailSidePx,
        ushort heightPx = Validator.ThumbnailSidePx,
        ushort shavePx = Validator.ThumbnailShavePx,
        bool fill = true,
        bool smartCrop = false,
        CancellationToken cancellationToken = default);

    /// <summary>
    ///   Checks whether Thumbnailer service is healthy or not.
    /// </summary>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>True if service is healthy, false otherwise.</returns>
    Task<bool> IsHealthyAsync(CancellationToken cancellationToken = default);

    /// <summary>
    ///   Returns true if given content type is supported by media file optimization with
    ///   specified mode, false otherwise.
    /// </summary>
    /// <param name="contentType">Content type.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>
    ///   True if given content type is supported by media file optimization, false otherwise.
    /// </returns>
    Task<bool> IsMediaOptimizationSupportedAsync(string contentType, CancellationToken cancellationToken = default);

    /// <summary>
    ///   Returns true if given content type is supported by thumbnail generation, false otherwise.
    /// </summary>
    /// <param name="contentType">Content type.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>
    ///   True if given content type is supported by thumbnail generation, false otherwise.
    /// </returns>
    Task<bool> IsThumbnailGenerationSupportedAsync(string contentType, CancellationToken cancellationToken = default);

    /// <summary>
    ///   Optimizes given source media file.
    /// </summary>
    /// <param name="contents">File bytes.</param>
    /// <param name="contentType">
    ///   File content type. If not specified, it will be deduced from file signature: however,
    ///   please specify it if available.
    /// </param>
    /// <param name="fileName">
    ///   File name. If not specified, its value will be a random file name compatible with
    ///   specified (or deduced) content type.
    /// </param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An optimized version of given source media file.</returns>
    Task<byte[]> OptimizeMediaAsync(
        byte[] contents,
        string? contentType = default,
        string? fileName = default,
        CancellationToken cancellationToken = default);

    /// <summary>
    ///   Optimizes given source media file.
    /// </summary>
    /// <param name="fileUri">Source file URI.</param>
    /// <param name="cancellationToken">Cancellation token.</param>
    /// <returns>An optimized version of given source media file.</returns>
    Task<byte[]> OptimizeMediaAsync(Uri fileUri, CancellationToken cancellationToken = default);
}
