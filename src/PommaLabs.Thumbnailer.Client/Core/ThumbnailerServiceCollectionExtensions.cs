﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace Microsoft.Extensions.DependencyInjection;

using Microsoft.Extensions.DependencyInjection.Extensions;
using PommaLabs.Thumbnailer.Client;
using PommaLabs.Thumbnailer.Client.Services.Clients;

/// <summary>
///   Registrations of <see cref="IThumbnailerClient"/> implementations.
/// </summary>
public static class ThumbnailerServiceCollectionExtensions
{
    /// <summary>
    ///   Registers <see cref="ConcreteThumbnailerClient"/> as singleton implementation of <see cref="IThumbnailerClient"/>.
    /// </summary>
    /// <param name="services">Service collection.</param>
    /// <returns>Modified service collection.</returns>
    public static IServiceCollection AddThumbnailerClient(this IServiceCollection services)
    {
        services.TryAddSingleton<IThumbnailerClient, ConcreteThumbnailerClient>();
        return services;
    }
}
