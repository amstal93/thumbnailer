﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Client.Core;

using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using PommaLabs.MimeTypes;

/// <summary>
///   Default parameter values and validations.
/// </summary>
internal sealed class Validator
{
    /// <summary>
    ///   Thumbnail shave pixels.
    /// </summary>
    public const ushort ThumbnailShavePx = 0;

    /// <summary>
    ///   Thumbnail side pixels.
    /// </summary>
    public const ushort ThumbnailSidePx = 256;

    /// <summary>
    ///   Content types **not** supported by thumbnail generation.
    /// </summary>
    private static readonly HashSet<string> s_contentTypesNotSupportedByThumbnailGeneration = new(StringComparer.OrdinalIgnoreCase)
    {
        MimeTypeMap.APPLICATION.OCTET_STREAM,
        MimeTypeMap.APPLICATION.X_MS_DOS_EXECUTABLE,
        MimeTypeMap.APPLICATION.X_MSDOS_PROGRAM,
        MimeTypeMap.APPLICATION.X_MSDOWNLOAD,
        MimeTypeMap.IMAGE.VND_ADOBE_PHOTOSHOP,
    };

    /// <summary>
    ///   Content types supported by media optimization.
    /// </summary>
    private static readonly HashSet<string> s_contentTypesSupportedByMediaOptimization = new(StringComparer.OrdinalIgnoreCase)
    {
        MimeTypeMap.IMAGE.GIF,
        MimeTypeMap.IMAGE.JPEG,
        MimeTypeMap.IMAGE.PNG,
        MimeTypeMap.IMAGE.SVG_XML,
        MimeTypeMap.IMAGE.WEBP,
        MimeTypeMap.VIDEO.MP4,
    };

    /// <summary>
    ///   Logger.
    /// </summary>
    private readonly ILogger _logger;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="logger">Logger.</param>
    public Validator(ILogger logger)
    {
        _logger = logger;
    }

    /// <summary>
    ///   Validates content type for media file optimization.
    /// </summary>
    /// <param name="contentType">Content type.</param>
    /// <param name="throw">True if it should throw on fail, false otherwise.</param>
    public bool ValidateContentTypeForMediaOptimization(string contentType, bool @throw)
    {
        if (s_contentTypesSupportedByMediaOptimization.Contains(contentType)) return true;
        if (!@throw) return false;

        _logger.LogError("An invalid content type was specified as input: {ContentType}", contentType);
        throw new NotSupportedException($"Content type '{contentType}' is not supported by media optimization");
    }

    /// <summary>
    ///   Validates content type for thumbnail generation.
    /// </summary>
    /// <param name="contentType">Content type.</param>
    /// <param name="throw">True if it should throw on fail, false otherwise.</param>
    public bool ValidateContentTypeForThumbnailGeneration(string contentType, bool @throw)
    {
        if (!s_contentTypesNotSupportedByThumbnailGeneration.Contains(contentType)) return true;
        if (!@throw) return false;

        _logger.LogError("An invalid content type was specified as input: {ContentType}", contentType);
        throw new NotSupportedException($"Content type '{contentType}' is not supported by thumbnail generation");
    }

    /// <summary>
    ///   Validates file parameters.
    /// </summary>
    /// <param name="bytes">File bytes.</param>
    public bool ValidateFileBytes(byte[] bytes)
    {
        if (bytes == null || bytes.Length == 0)
        {
            _logger.LogError("Null or empty file bytes have been received");
            throw new ArgumentNullException(nameof(bytes));
        }
        return true;
    }

    /// <summary>
    ///   Validates file URI parameter.
    /// </summary>
    /// <param name="fileUri">File URI.</param>
    public bool ValidateFileUri(Uri fileUri)
    {
        if (fileUri == null)
        {
            throw new ArgumentNullException(nameof(fileUri));
        }
        if (fileUri.IsFile)
        {
            _logger.LogError("A file URI was specified as input: {FileUri}", fileUri);
            throw new NotSupportedException("File URIs are not allowed");
        }
        if (fileUri.IsLoopback)
        {
            _logger.LogError("A loopback URI was specified as input: {FileUri}", fileUri);
            throw new NotSupportedException("Loopback URIs are not allowed");
        }
        if (!fileUri.IsAbsoluteUri)
        {
            _logger.LogError("A relative URI was specified as input: {FileUri}", fileUri);
            throw new NotSupportedException("Relative URIs are not allowed");
        }
        return true;
    }
}
