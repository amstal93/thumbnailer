﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Client;

using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

/// <summary>
///   Health check for Thumbnailer service.
/// </summary>
public sealed class ThumbnailerHealthCheck : IHealthCheck
{
    private readonly IThumbnailerClient _thumbnailerClient;

    /// <summary>
    ///   Builds the health check for Thumbnailer service.
    /// </summary>
    /// <param name="thumbnailerClient">Thumbnailer client.</param>
    public ThumbnailerHealthCheck(IThumbnailerClient thumbnailerClient)
    {
        _thumbnailerClient = thumbnailerClient;
    }

    /// <inheritdoc/>
    public async Task<HealthCheckResult> CheckHealthAsync(
        HealthCheckContext context, CancellationToken cancellationToken = default)
    {
        var isHealthy = await _thumbnailerClient.IsHealthyAsync(cancellationToken).ConfigureAwait(false);
        return isHealthy ? HealthCheckResult.Healthy() : HealthCheckResult.Unhealthy();
    }
}
