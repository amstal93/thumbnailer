﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Client.Services.Clients;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly;
using Polly.Retry;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Client.Models.DTO.Public;
using PommaLabs.Thumbnailer.Client.Models.Enumerations;
using PommaLabs.Thumbnailer.Client.Resources;

/// <summary>
///   Standard Thumbnailer client.
/// </summary>
public sealed class ConcreteThumbnailerClient : IThumbnailerClient
{
    private const string ApiKeyHeaderName = "X-Api-Key";
    private const string HealthCheckEndpoint = "health";
    private const string JobsEndpoint = "api/v2/jobs";
    private const string MediaOptimizationEndpoint = "api/v2/optimize";
    private const string ThumbnailGenerationEndpoint = "api/v2/thumbnail";

    private static readonly HashSet<int> s_statusCodesOfTransientErrors = new() { 424, 429 };

    private readonly IOptions<ThumbnailerClientConfiguration> _clientConfiguration;
    private readonly AsyncRetryPolicy _retryPolicy;
    private readonly Validator _validator;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="clientConfiguration">Thumbnailer client configuration.</param>
    /// <param name="logger">Logger.</param>
    public ConcreteThumbnailerClient(
        IOptions<ThumbnailerClientConfiguration> clientConfiguration,
        ILogger<ConcreteThumbnailerClient> logger)
    {
        _clientConfiguration = clientConfiguration;
        _validator = new Validator(logger);

        // Wait 2 seconds after first error, then 4, 8, etc.
        _retryPolicy = Policy
            .Handle<FlurlHttpException>(ex =>
                ex is FlurlHttpTimeoutException ||
                !ex.Call.Completed ||
                s_statusCodesOfTransientErrors.Contains(ex.Call.Response.StatusCode))
            .WaitAndRetryAsync(clientConfiguration.Value.RetryCount,
                retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)));
    }

    /// <inheritdoc/>
    public async Task<byte[]> GenerateThumbnailAsync(
        Uri fileUri,
        ushort widthPx = Validator.ThumbnailSidePx,
        ushort heightPx = Validator.ThumbnailSidePx,
        ushort shavePx = Validator.ThumbnailShavePx,
        bool fill = true,
        bool smartCrop = false,
        CancellationToken cancellationToken = default)
    {
        _validator.ValidateFileUri(fileUri);

        var endpoint = Url.Combine(_clientConfiguration.Value.BaseUri.AbsoluteUri, ThumbnailGenerationEndpoint);
        using (StartClientActivity(ActivityNames.ThumbnailGeneration, HttpMethod.Get, endpoint))
        {
            var job = await EvaluateResponseAsync(OperationType.ThumbnailGeneration, async () => await endpoint
                    .WithTimeout(_clientConfiguration.Value.Timeout)
                    .WithHeader(ApiKeyHeaderName, _clientConfiguration.Value.ApiKey)
                    .SetQueryParam(nameof(fileUri), fileUri.AbsoluteUri)
                    .SetQueryParam(nameof(widthPx), widthPx.ToString(CultureInfo.InvariantCulture))
                    .SetQueryParam(nameof(heightPx), heightPx.ToString(CultureInfo.InvariantCulture))
                    .SetQueryParam(nameof(shavePx), shavePx.ToString(CultureInfo.InvariantCulture))
                    .SetQueryParam(nameof(fill), fill.ToString(CultureInfo.InvariantCulture))
                    .SetQueryParam(nameof(smartCrop), smartCrop.ToString(CultureInfo.InvariantCulture))
                    .GetJsonAsync<JobDetails>(cancellationToken)
                    .ConfigureAwait(false), cancellationToken)
                .ConfigureAwait(false);

            await CheckJobStatusAsync(job, cancellationToken).ConfigureAwait(false);
            return await DownloadJobResultAsync(job, cancellationToken).ConfigureAwait(false);
        }
    }

    /// <inheritdoc/>
    public async Task<byte[]> GenerateThumbnailAsync(
        byte[] contents,
        string? contentType = default,
        string? fileName = default,
        ushort widthPx = Validator.ThumbnailSidePx,
        ushort heightPx = Validator.ThumbnailSidePx,
        ushort shavePx = Validator.ThumbnailShavePx,
        bool fill = true,
        bool smartCrop = false,
        CancellationToken cancellationToken = default)
    {
        if (contentType == null)
        {
            (contentType, fileName) = MimeTypeHelper.GetMimeTypeAndExtension(contents, fileName);
        }

        _validator.ValidateFileBytes(contents);
        _validator.ValidateContentTypeForThumbnailGeneration(contentType, @throw: true);

        fileName = AdjustFileName(contentType, fileName);

        var endpoint = Url.Combine(_clientConfiguration.Value.BaseUri.AbsoluteUri, ThumbnailGenerationEndpoint);
        using (StartClientActivity(ActivityNames.ThumbnailGeneration, HttpMethod.Post, endpoint))
        {
            var job = await EvaluateResponseAsync(OperationType.ThumbnailGeneration, async () => await endpoint
                    .WithTimeout(_clientConfiguration.Value.Timeout)
                    .WithHeader(ApiKeyHeaderName, _clientConfiguration.Value.ApiKey)
                    .SetQueryParam(nameof(widthPx), widthPx.ToString(CultureInfo.InvariantCulture))
                    .SetQueryParam(nameof(heightPx), heightPx.ToString(CultureInfo.InvariantCulture))
                    .SetQueryParam(nameof(shavePx), shavePx.ToString(CultureInfo.InvariantCulture))
                    .SetQueryParam(nameof(fill), fill.ToString(CultureInfo.InvariantCulture))
                    .SetQueryParam(nameof(smartCrop), smartCrop.ToString(CultureInfo.InvariantCulture))
                    .PostMultipartAsync(mp => mp.AddFile("file", new MemoryStream(contents), fileName, contentType),
                        cancellationToken)
                    .ReceiveJson<JobDetails>()
                    .ConfigureAwait(false), cancellationToken)
                .ConfigureAwait(false);

            await CheckJobStatusAsync(job, cancellationToken).ConfigureAwait(false);
            return await DownloadJobResultAsync(job, cancellationToken).ConfigureAwait(false);
        }
    }

    /// <inheritdoc/>
    public async Task<bool> IsHealthyAsync(CancellationToken cancellationToken = default)
    {
        var endpoint = Url.Combine(_clientConfiguration.Value.BaseUri.AbsoluteUri, HealthCheckEndpoint);
        using (StartClientActivity(ActivityNames.HealthCheck, HttpMethod.Get, endpoint))
        {
            var response = await endpoint
                .WithTimeout(_clientConfiguration.Value.Timeout)
                .AllowAnyHttpStatus()
                .GetAsync(cancellationToken)
                .ConfigureAwait(false);

            return response.StatusCode == (int)HttpStatusCode.OK;
        }
    }

    /// <inheritdoc/>
    public Task<bool> IsMediaOptimizationSupportedAsync(string contentType, CancellationToken cancellationToken = default)
    {
        return Task.FromResult(_validator.ValidateContentTypeForMediaOptimization(contentType, @throw: false));
    }

    /// <inheritdoc/>
    public Task<bool> IsThumbnailGenerationSupportedAsync(string contentType, CancellationToken cancellationToken = default)
    {
        return Task.FromResult(_validator.ValidateContentTypeForThumbnailGeneration(contentType, @throw: false));
    }

    /// <inheritdoc/>
    public async Task<byte[]> OptimizeMediaAsync(Uri fileUri, CancellationToken cancellationToken = default)
    {
        _validator.ValidateFileUri(fileUri);

        var endpoint = Url.Combine(_clientConfiguration.Value.BaseUri.AbsoluteUri, MediaOptimizationEndpoint);
        using (StartClientActivity(ActivityNames.MediaOptimization, HttpMethod.Get, endpoint))
        {
            var job = await EvaluateResponseAsync(OperationType.MediaOptimization, async () => await endpoint
                    .WithTimeout(_clientConfiguration.Value.Timeout)
                    .WithHeader(ApiKeyHeaderName, _clientConfiguration.Value.ApiKey)
                    .SetQueryParam(nameof(fileUri), fileUri.AbsoluteUri)
                    .GetJsonAsync<JobDetails>(cancellationToken)
                    .ConfigureAwait(false), cancellationToken)
                .ConfigureAwait(false);

            await CheckJobStatusAsync(job, cancellationToken).ConfigureAwait(false);
            return await DownloadJobResultAsync(job, cancellationToken).ConfigureAwait(false);
        }
    }

    /// <inheritdoc/>
    public async Task<byte[]> OptimizeMediaAsync(
        byte[] contents,
        string? contentType = default,
        string? fileName = default,
        CancellationToken cancellationToken = default)
    {
        if (contentType == null)
        {
            (contentType, fileName) = MimeTypeHelper.GetMimeTypeAndExtension(contents, fileName);
        }

        _validator.ValidateFileBytes(contents);
        _validator.ValidateContentTypeForMediaOptimization(contentType, @throw: true);

        fileName = AdjustFileName(contentType, fileName);

        var endpoint = Url.Combine(_clientConfiguration.Value.BaseUri.AbsoluteUri, MediaOptimizationEndpoint);
        using (StartClientActivity(ActivityNames.MediaOptimization, HttpMethod.Post, endpoint))
        {
            var job = await EvaluateResponseAsync(OperationType.MediaOptimization, async () => await endpoint
                    .WithTimeout(_clientConfiguration.Value.Timeout)
                    .WithHeader(ApiKeyHeaderName, _clientConfiguration.Value.ApiKey)
                    .PostMultipartAsync(mp => mp.AddFile("file", new MemoryStream(contents), fileName, contentType),
                        cancellationToken)
                    .ReceiveJson<JobDetails>()
                    .ConfigureAwait(false), cancellationToken)
                .ConfigureAwait(false);

            await CheckJobStatusAsync(job, cancellationToken).ConfigureAwait(false);
            return await DownloadJobResultAsync(job, cancellationToken).ConfigureAwait(false);
        }
    }

    private static string AdjustFileName(string contentType, string? fileName)
    {
        var extension = Path.GetExtension(fileName);
        return $"f{(!string.IsNullOrWhiteSpace(extension) ? extension : MimeTypeMap.GetExtension(contentType))}";
    }

    private static Activity? StartClientActivity(string activityName, HttpMethod httpMethod, string httpUrl)
    {
        var activity = ThumbnailerClientActivitySource.Instance.StartActivity(activityName, ActivityKind.Client);
        if (activity == null)
        {
            return activity;
        }

        activity.AddTag("http.method", httpMethod.Method);
        activity.AddTag("http.url", httpUrl);
        return activity;
    }

    private async Task CheckJobStatusAsync(JobDetails job, CancellationToken cancellationToken)
    {
        var stopwatch = Stopwatch.StartNew();
        var endpoint = Url.Combine(_clientConfiguration.Value.BaseUri.AbsoluteUri, JobsEndpoint)
            .AppendPathSegment(job.JobId);

        while (true)
        {
            using (StartClientActivity(ActivityNames.JobRetrieval, HttpMethod.Get, endpoint))
            {
                job = await EvaluateResponseAsync(OperationType.JobRetrieval, async () => await endpoint
                        .WithTimeout(_clientConfiguration.Value.Timeout)
                        .WithHeader(ApiKeyHeaderName, _clientConfiguration.Value.ApiKey)
                        .GetJsonAsync<JobDetails>(cancellationToken: cancellationToken)
                        .ConfigureAwait(false), cancellationToken)
                    .ConfigureAwait(false);
            }

            switch (job.Status)
            {
                case JobStatus.Queued:
                case JobStatus.Processing:
                    break;

                case JobStatus.Processed:
                    return;

                case JobStatus.Failed:
                    throw new ThumbnailerClientException(job.FailureReason!, OperationType.JobRetrieval);

                default:
                    throw new ThumbnailerClientException("Async job has an invalid status", OperationType.JobRetrieval);
            }

            if (stopwatch.Elapsed > _clientConfiguration.Value.JobTimeout)
            {
                throw new ThumbnailerClientException("Async job was aborted or timed out", OperationType.JobRetrieval);
            }

            await Task.Delay(500, cancellationToken).ConfigureAwait(false);
        }
    }

    private async Task<byte[]> DownloadJobResultAsync(JobDetails job, CancellationToken cancellationToken)
    {
        var endpoint = Url.Combine(_clientConfiguration.Value.BaseUri.AbsoluteUri, JobsEndpoint)
            .AppendPathSegment(job.JobId)
            .AppendPathSegment("download");

        using (StartClientActivity(ActivityNames.JobResultDownload, HttpMethod.Get, endpoint))
        {
            return await EvaluateResponseAsync(OperationType.JobRetrieval, async () => await endpoint
                    .WithTimeout(_clientConfiguration.Value.Timeout)
                    .WithHeader(ApiKeyHeaderName, _clientConfiguration.Value.ApiKey)
                    .GetBytesAsync(cancellationToken)
                    .ConfigureAwait(false), cancellationToken)
                .ConfigureAwait(false);
        }
    }

    private async Task<T> EvaluateResponseAsync<T>(
        OperationType operationType, Func<Task<T>> httpCall,
        CancellationToken cancellationToken)
    {
        try
        {
            return await _retryPolicy.ExecuteAsync(
                async _ => await httpCall().ConfigureAwait(false),
                cancellationToken).ConfigureAwait(false);
        }
        catch (FlurlHttpTimeoutException httpTimeoutException)
        {
            throw new ThumbnailerClientException("Request was aborted or timed out", httpTimeoutException,
                operationType, default);
        }
        catch (FlurlHttpException httpException)
        {
            var errorResponse = await httpException.GetResponseJsonAsync().ConfigureAwait(false);

            string message;
            if (errorResponse is IDictionary<string, object> expandoObject && expandoObject.ContainsKey("detail"))
            {
                message = errorResponse.detail;
            }
            else
            {
                message = await httpException.GetResponseStringAsync().ConfigureAwait(false);
            }

            throw new ThumbnailerClientException(message, httpException, operationType,
                (HttpStatusCode)httpException.Call.Response.StatusCode);
        }
    }
}
