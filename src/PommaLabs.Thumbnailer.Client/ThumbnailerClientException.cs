﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.Client;

using System;
using System.Net;
using System.Runtime.Serialization;
using PommaLabs.Thumbnailer.Client.Models.Enumerations;

/// <summary>
///   Represents an error produced by Thumbnailer service.
/// </summary>
[Serializable]
public class ThumbnailerClientException : Exception
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="message">Message.</param>
    /// <param name="operationType">Operation type.</param>
    public ThumbnailerClientException(
        string message, OperationType operationType)
        : base(message)
    {
        Data[nameof(OperationType)] = operationType;
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="message">Message.</param>
    /// <param name="innerException">Inner exception.</param>
    /// <param name="operationType">Operation type.</param>
    /// <param name="httpStatusCode">HTTP status code.</param>
    public ThumbnailerClientException(
        string message, Exception innerException,
        OperationType operationType, HttpStatusCode? httpStatusCode)
        : base(message, innerException)
    {
        Data[nameof(OperationType)] = operationType;
        Data[nameof(HttpStatusCode)] = httpStatusCode;
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    public ThumbnailerClientException()
    {
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="message">Message</param>
    public ThumbnailerClientException(string message)
        : base(message)
    {
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="message">Message.</param>
    /// <param name="innerException">Inner exception.</param>
    public ThumbnailerClientException(string message, Exception innerException)
        : base(message, innerException)
    {
    }

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="info">Serialization info.</param>
    /// <param name="context">Streaming context.</param>
    protected ThumbnailerClientException(SerializationInfo info, StreamingContext context)
        : base(info, context)
    {
        info.AddValue(nameof(HttpStatusCode), HttpStatusCode);
        info.AddValue(nameof(OperationType), OperationType);
    }

    /// <summary>
    ///   HTTP status code.
    /// </summary>
    public HttpStatusCode? HttpStatusCode => Data[nameof(HttpStatusCode)] as HttpStatusCode?;

    /// <summary>
    ///   Operation type.
    /// </summary>
    public OperationType? OperationType => Data[nameof(OperationType)] as OperationType?;
}
