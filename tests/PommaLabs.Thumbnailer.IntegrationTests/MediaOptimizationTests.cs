﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.IntegrationTests;

using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Client;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Client.Models.Enumerations;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using VerifyNUnit;

internal sealed class MediaOptimizationTests : ThumbnailerTestsBase
{
    private const string MediaOptimizationV1Endpoint = "api/v1/optimize";

    private static readonly HashSet<string> s_unverifiableExtensions = new()
    {
        MimeTypeMap.Extensions.MP4,
    };

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.MediaOptimization })]
    public async Task Base64_ValidMedia_V1_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var endpoint = Url.Combine(clientConfiguration.Value.BaseUri.AbsoluteUri, MediaOptimizationV1Endpoint, "base64");
        var fileBytes = File.ReadAllBytes(sampleFile.LocalPath);

        // Act
        var isSupported = await client.IsMediaOptimizationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await endpoint
            .WithHeader(ApiKeyHeaderName, clientConfiguration.Value.ApiKey)
            .PostJsonAsync(new FileDetails { FileName = sampleFile.Name, Contents = fileBytes, ContentType = sampleFile.ContentType })
            .ReceiveBytes();

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, resultExtension) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(sampleFile.ContentType));
        if (!s_unverifiableExtensions.Contains(resultExtension))
        {
            await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile));
        }
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.MediaOptimization })]
    public async Task LocalPath_ValidMedia_V1_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var endpoint = Url.Combine(clientConfiguration.Value.BaseUri.AbsoluteUri, MediaOptimizationV1Endpoint);
        var fileBytes = File.ReadAllBytes(sampleFile.LocalPath);

        // Act
        var isSupported = await client.IsMediaOptimizationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await endpoint
            .WithHeader(ApiKeyHeaderName, clientConfiguration.Value.ApiKey)
            .PostMultipartAsync(mp => mp.AddFile("file", new MemoryStream(fileBytes), sampleFile.Name, sampleFile.ContentType))
            .ReceiveBytes();

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, resultExtension) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(sampleFile.ContentType));
        if (!s_unverifiableExtensions.Contains(resultExtension))
        {
            await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile));
        }
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.MediaOptimization })]
    public async Task LocalPath_ValidMedia_V2_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);
        var fileBytes = File.ReadAllBytes(sampleFile.LocalPath);

        // Act
        var isSupported = await client.IsMediaOptimizationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await client.OptimizeMediaAsync(fileBytes, fileName: sampleFile.Name);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, resultExtension) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(sampleFile.ContentType));
        if (!s_unverifiableExtensions.Contains(resultExtension))
        {
            await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile));
        }
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.MediaOptimization })]
    public async Task RemoteUri_ValidMedia_V1_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var endpoint = Url.Combine(clientConfiguration.Value.BaseUri.AbsoluteUri, MediaOptimizationV1Endpoint);

        // Act
        var isSupported = await client.IsMediaOptimizationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await endpoint
            .WithHeader(ApiKeyHeaderName, clientConfiguration.Value.ApiKey)
            .SetQueryParam("fileUri", sampleFile.RemoteUri)
            .GetBytesAsync();

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, resultExtension) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(sampleFile.ContentType));
        if (!s_unverifiableExtensions.Contains(resultExtension))
        {
            await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile));
        }
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.MediaOptimization })]
    public async Task RemoteUri_ValidMedia_V2_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);

        // Act
        var isSupported = await client.IsMediaOptimizationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await client.OptimizeMediaAsync(sampleFile.RemoteUri);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, resultExtension) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(sampleFile.ContentType));
        if (!s_unverifiableExtensions.Contains(resultExtension))
        {
            await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile));
        }
    }
}
