CREATE TABLE `api_keys` (
	`id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Automatically generated ID.',
	`name` VARCHAR(100) NOT NULL COMMENT 'Descriptive name of the API key.' COLLATE 'utf8mb4_unicode_ci',
	`value` VARCHAR(32) NOT NULL COMMENT 'The API key.' COLLATE 'utf8mb4_unicode_ci',
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP() COMMENT 'Creation timestamp.',
	`expires_at` TIMESTAMP NULL COMMENT 'Optional expiration timestamp.',
	`temporary` BOOLEAN NOT NULL DEFAULT '0' COMMENT 'If true, API key will be automatically removed once it will be expired.',
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `uk_api_keys` (`value`) USING BTREE
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=InnoDB
;

INSERT INTO `api_keys` (`name`, `value`) VALUES ('INTEGRATION TESTS', '__INTEGRATION_TESTS__');

CREATE TABLE `cache_entries` (
	`kvle_id` BIGINT NOT NULL AUTO_INCREMENT COMMENT 'Automatically generated ID.',
	`kvle_hash` BIGINT NOT NULL COMMENT 'Hash of partition and key.',
	`kvle_expiry` BIGINT NOT NULL COMMENT 'When the entry will expire, expressed as seconds after UNIX epoch.',
	`kvle_interval` BIGINT NOT NULL COMMENT 'How many seconds should be used to extend expiry time when the entry is retrieved.',
	`kvle_value` MEDIUMBLOB NOT NULL COMMENT 'Serialized and optionally compressed content of this entry.',
	`kvle_compressed` TINYINT NOT NULL COMMENT 'Whether the entry content was compressed or not.',
	`kvle_partition` VARCHAR(2000) NOT NULL COMMENT 'A partition holds a group of related keys.' COLLATE 'utf8mb4_unicode_ci',
	`kvle_key` VARCHAR(2000) NOT NULL COMMENT 'A key uniquely identifies an entry inside a partition.' COLLATE 'utf8mb4_unicode_ci',
	`kvle_creation` BIGINT NOT NULL COMMENT 'When the entry was created, expressed as seconds after UNIX epoch.',
	`kvle_parent_hash0` BIGINT NULL COMMENT 'Optional parent entry hash, used to link entries in a hierarchical way.',
	`kvle_parent_key0` VARCHAR(2000) NULL COMMENT 'Optional parent entry key, used to link entries in a hierarchical way.' COLLATE 'utf8mb4_unicode_ci',
	`kvle_parent_hash1` BIGINT NULL COMMENT 'Optional parent entry hash, used to link entries in a hierarchical way.',
	`kvle_parent_key1` VARCHAR(2000) NULL COMMENT 'Optional parent entry key, used to link entries in a hierarchical way.' COLLATE 'utf8mb4_unicode_ci',
	`kvle_parent_hash2` BIGINT NULL COMMENT 'Optional parent entry hash, used to link entries in a hierarchical way.',
	`kvle_parent_key2` VARCHAR(2000) NULL COMMENT 'Optional parent entry key, used to link entries in a hierarchical way.' COLLATE 'utf8mb4_unicode_ci',
	PRIMARY KEY (`kvle_id`) USING BTREE,
	UNIQUE INDEX `uk_cache_entries` (`kvle_hash`) USING BTREE
)
COLLATE='utf8mb4_unicode_ci'
ENGINE=InnoDB
ROW_FORMAT=DYNAMIC
;
