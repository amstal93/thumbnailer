﻿// Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

namespace PommaLabs.Thumbnailer.IntegrationTests;

using System.IO;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NUnit.Framework;
using PommaLabs.MimeTypes;
using PommaLabs.Thumbnailer.Client;
using PommaLabs.Thumbnailer.Client.Core;
using PommaLabs.Thumbnailer.Client.Models.Enumerations;
using PommaLabs.Thumbnailer.Models.DTO.Public;
using VerifyNUnit;

internal sealed class ThumbnailGenerationTests : ThumbnailerTestsBase
{
    private const string ThumbnailGenerationV1Endpoint = "api/v1/thumbnail";

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task Base64_256x256WithFill_V1_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var endpoint = Url.Combine(clientConfiguration.Value.BaseUri.AbsoluteUri, ThumbnailGenerationV1Endpoint, "base64");
        var fileBytes = File.ReadAllBytes(sampleFile.LocalPath);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await endpoint
            .WithHeader(ApiKeyHeaderName, clientConfiguration.Value.ApiKey)
            .PostJsonAsync(new FileDetails { FileName = sampleFile.Name, Contents = fileBytes, ContentType = sampleFile.ContentType })
            .ReceiveBytes();

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile, "PNG"));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task LocalPath_256x256WithFill_V1_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var endpoint = Url.Combine(clientConfiguration.Value.BaseUri.AbsoluteUri, ThumbnailGenerationV1Endpoint);
        var fileBytes = File.ReadAllBytes(sampleFile.LocalPath);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await endpoint
            .WithHeader(ApiKeyHeaderName, clientConfiguration.Value.ApiKey)
            .PostMultipartAsync(mp => mp.AddFile("file", new MemoryStream(fileBytes), sampleFile.Name, sampleFile.ContentType))
            .ReceiveBytes();

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile, "PNG"));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task LocalPath_256x256WithFill_V2_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);
        var fileBytes = File.ReadAllBytes(sampleFile.LocalPath);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await client.GenerateThumbnailAsync(fileBytes, fileName: sampleFile.Name);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile, "PNG"));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task LocalPath_256x256WithSmartCrop_V2_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);
        var fileBytes = File.ReadAllBytes(sampleFile.LocalPath);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await client.GenerateThumbnailAsync(fileBytes, fileName: sampleFile.Name, smartCrop: true);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile, "PNG"));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task LocalPath_384x384ShavedOff42_V2_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);
        var fileBytes = File.ReadAllBytes(sampleFile.LocalPath);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await client.GenerateThumbnailAsync(fileBytes, fileName: sampleFile.Name, widthPx: 384, heightPx: 384, shavePx: 42);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile, "PNG"));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task LocalPath_512x512WithoutFill_V2_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);
        var fileBytes = File.ReadAllBytes(sampleFile.LocalPath);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await client.GenerateThumbnailAsync(fileBytes, fileName: sampleFile.Name, widthPx: 512, heightPx: 512, fill: false);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile, "PNG"));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task RemoteUri_256x256WithFill_V1_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);
        var clientConfiguration = Services.ServiceProvider.GetRequiredService<IOptions<ThumbnailerClientConfiguration>>();
        var endpoint = Url.Combine(clientConfiguration.Value.BaseUri.AbsoluteUri, ThumbnailGenerationV1Endpoint);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await endpoint
            .WithHeader(ApiKeyHeaderName, clientConfiguration.Value.ApiKey)
            .SetQueryParam("fileUri", sampleFile.RemoteUri)
            .GetBytesAsync();

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile, "PNG"));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task RemoteUri_256x256WithFill_V2_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await client.GenerateThumbnailAsync(sampleFile.RemoteUri);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile, "PNG"));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task RemoteUri_256x256WithSmartCrop_V2_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await client.GenerateThumbnailAsync(sampleFile.RemoteUri, smartCrop: true);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile, "PNG"));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task RemoteUri_384x384ShavedOff42_V2_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await client.GenerateThumbnailAsync(sampleFile.RemoteUri, widthPx: 384, heightPx: 384, shavePx: 42);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile, "PNG"));
    }

    [TestCaseSource(nameof(ReadSampleFiles), new object[] { OperationType.ThumbnailGeneration })]
    public async Task RemoteUri_512x512WithoutFill_V2_ShouldGenerateValidResult(SampleFile sampleFile)
    {
        // Arrange
        var client = Services.ServiceProvider.GetRequiredService<IThumbnailerClient>();
        await EnsureServiceIsHealthyAsync(client);

        // Act
        var isSupported = await client.IsThumbnailGenerationSupportedAsync(sampleFile.ContentType);
        var resultBytes = await client.GenerateThumbnailAsync(sampleFile.RemoteUri, widthPx: 512, heightPx: 512, fill: false);

        // Assert
        Assert.That(isSupported, Is.True);
        Assert.That(resultBytes.Length, Is.GreaterThan(0));
        var (resultMimeType, _) = MimeTypeHelper.GetMimeTypeAndExtension(resultBytes, sampleFile.Name);
        Assert.That(resultMimeType, Is.EqualTo(MimeTypeMap.IMAGE.PNG));
        await Verifier.Verify(resultBytes, CreateVerifySettings(sampleFile, "PNG"));
    }
}
