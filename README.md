# Thumbnailer

[![License: MIT][project-license-badge]][project-license]
[![Donate][paypal-donations-badge]][paypal-donations]
[![Docker Stars][docker-stars-badge]][docker-repository]
[![Docker Pulls][docker-pulls-badge]][docker-repository]
[![Docs][doxygen-docs-badge]][doxygen-docs]
[![NuGet version][nuget-version-badge]][nuget-package]
[![NuGet downloads][nuget-downloads-badge]][nuget-package]

[![standard-readme compliant][github-standard-readme-badge]][github-standard-readme]
[![GitLab pipeline status][gitlab-pipeline-status-badge]][gitlab-pipelines]
[![Quality gate][sonar-quality-gate-badge]][sonar-website]
[![Code coverage][sonar-coverage-badge]][sonar-website]
[![Renovate enabled][renovate-badge]][renovate-website]

Web service which exposes endpoints to generate file thumbnails and to optimize media files.

Thumbnailer heavily relies on many open source command line tools. Just to name a few:

- [`ffmpeg`][ffmpeg-website], used to generate video thumbnails and to optimize MP4 files.
- [`GraphicsMagick`][graphicsmagick-website], used to generate image thumbnails.
- [`LibreOffice`][libreoffice-website], used to convert Office files to images.
- [`PhantomJS`][phantomjs-website], used to convert HTML files to images.
- [`pngquant`][pngquant-website], used to optimize PNG images and file thumbnails.

Web service runs on ASP.NET Core and this repository contains a [.NET Standard client][nuget-package]
which can be used to interact with it.

## Table of Contents

- [Install](#install)
  - [Tags](#tags)
  - [Configuration](#configuration)
  - [Logging](#logging)
  - [Database](#database)
  - [Persistence](#persistence)
- [Usage](#usage)
  - [Synchronous API (v1)](#synchronous-api-v1)
  - [Asynchronous API (v2)](#asynchronous-api-v2)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
  - [Editing](#editing)
  - [Restoring dependencies](#restoring-dependencies)
  - [Starting development server](#starting-development-server)
  - [Running tests](#running-tests)
  - [Building Docker image](#building-docker-image)
- [License](#license)

## Install

Thumbnailer web service is provided as a Docker image hosted on [Docker Hub][docker-repository].

You can quickly start a local test instance with following command:

```bash
docker run -it -e Security__AllowAnonymousAccess=true -p 8080:8080 pommalabs/thumbnailer:latest
```

Local test instance will be listening on port 8080 and it will accept unauthenticated requests.
Please check the [Configuration](#configuration) section to find further information
about how the web service can be properly configured.

A .NET client, [PommaLabs.Thumbnailer.Client][nuget-package], is available as a NuGet package:

```bash
dotnet add package PommaLabs.Thumbnailer.Client
```

### Tags

Following tags are available:

- `latest`, based on `pommalabs/dotnet:5-aspnet`, contains the code found in `main` branch.
- `azure-appsvc`, based on `pommalabs/dotnet:5-azure-appsvc`, contains the code found in `main` branch.
  This tag is customized in order to support SSH connection exposed through Azure App Service.
- `preview`, based on `pommalabs/dotnet:5-aspnet`, contains the code found in `preview` branch.

Each tag is rebuilt every week.

### Configuration

Docker image can be configured using the following environment variables:

| Environment variable                             | Notes                                            | Default value |
|--------------------------------------------------|--------------------------------------------------|---------------|
| `Database__CacheLifetime`                        | How long each cache entry will live.             | 20 minutes    |
| `Database__ConnectionString`                     | Connection string of a SQL database.             |               |
| `Database__Provider`                             | Provider (`None`, `PostgreSql`, `SqlServer`).    | `None`        |
| `Database__SchemaName`                           | SQL schema name.                                 |               |
| `Security__AcceptApiKeysViaHeaderParameter`      | Accept API keys via header parameter.            | `true`        |
| `Security__AcceptApiKeysViaQueryStringParameter` | Accept API keys via query string parameter.      | `false`       |
| `Security__AllowAnonymousAccess`                 | Allow anonymous users to consume services.       | `false`       |
| `Security__ApiKeys__X__ExpiresAt`                | If not specified, API key will not expire.       |               |
| `Security__ApiKeys__X__Name`                     | Descriptive name of API key at index X.          |               |
| `Security__ApiKeys__X__Value`                    | Value of API key at index X.                     |               |
| `Security__AsyncProcessTimeout`                  | Timeout for low level processes (in async jobs). | 10 minutes    |
| `Security__EnableCaseSensitiveApiKeyValidation`  | Enable case sensitive API key validation.        | `false`       |
| `Security__FileDownloadTimeout`                  | Timeout for file download.                       | 30 seconds    |
| `Security__MaxFileDownloadSizeInBytes`           | How many bytes are allowed for remote downloads. | 64 MB         |
| `Security__MaxFileUploadSizeInBytes`             | How many bytes are allowed for uploads.          | 32 MB         |
| `Security__ProcessTimeout`                       | Timeout for low level processes (in sync calls). | 30 seconds    |
| `Website__Enabled`                               | Expose the public website.                       | `false`       |

### Logging

Web service writes log messages to the console and to the file system.
Log files are stored in the following directory, which is not configurable:

`/opt/app/Data/Logs`

Log files are rotated daily and a maximum number of 31 files is kept.

If an Azure Application Insights Instrumentation Key is specified using the
`APPINSIGHTS_INSTRUMENTATIONKEY` configuration, logs are also sent to that service.

### Database

Setup scripts for supported systems are available:

- [MySQL and MariaDB][thumbnailer-mysql-schema]
- [PostgreSQL][thumbnailer-postgresql-schema]
- [SQL Server][thumbnailer-sqlserver-schema]

You can customize those scripts according to your needs, but please remember that
if you change schema name, you should update `Database__SchemaName`
configuration property accordingly.

### Persistence

When an external database is configured, it is also used by the web service
to store cache entries using the [KVLite library][gitlab-kvlite].

Cache entries store the SHA384 of input files and the parameters used;
that data is linked to the location of the output file
in Thumbnailer temporary directory (`/tmp/thumbnailer`).

Moreover, distributed cache is also used to track job information for API v2.
Job IDs are encrypted using local keys (`/opt/app/Data/DataProtectionKeys`).

So, you might want to persist the following directories:

- `/tmp/thumbnailer`, where temporary files stay for at least
  as long as it has been specified with `Database__CacheLifetime` configuration.
- `/opt/app/Data/DataProtectionKeys`, where local encryption keys are stored.
- `/opt/app/Data/Logs`, where log files are written.

Persisting those directories is not mandatory at all.
The web service works fine without having them persisted.

However, persisting them might be useful for the following use cases:

- Web service receives a lot of requests and temporary files directory
  can grow quite a lot. In that case, a separate volume with proper size can be configured.
- Web service works in load balancing and it is configured to use an external DB.
  In that case, sharing the temporary files directory is required in order
  to let all instances be able to read cached outputs.
- Easier log files access. Having log files stored externally might make them easier
  to access with other tools.

## Usage

Please check OpenAPI docs on your instance (just visit the `/swagger` page)
or head to the [demo instance Swagger page][thumbnailer-swagger-index].
Each endpoint has been documented using the OpenAPI specification.

**Media optimization** supports following content types:

- `image/gif`
- `image/jpeg`
- `image/png`
- `image/svg+xml`
- `image/webp`
- `video/mp4`

**Thumbnail generation** supports quite a lot of content types.
Anyway, you can use the [test files][thumbnailer-samples] as a quick reference
to find out what the web service really supports.
Inside each "format" folder you will find a copy of the latest thumbnails, that is,
for each file you will find the most recent copy of its thumbnail.

However, not every content type for which a thumbnail could be "theoretically" generated is supported.
Some notable formats which are **not** supported are the following:

- PSD files represent a security issue since GraphicsMagick support is not complete.
  As a matter of fact, [their support in GraphicsMagick has been disabled][graphicsmagick-psd-disabled].

### Synchronous API (v1)

Synchronous API is exposed through [v1 endpoints][thumbnailer-swagger-v1] and,
as its name suggests, it allows to send synchronous HTTP requests for
thumbnail generation or media generation, where the output file is provided
as a response for each request.

This approach is quite simple but it is not recommended, because each HTTP
request should have a very short timeout and that fact does not work well with
the processing of large or complex files.
Anyway, this kind of API was the first one to be implemented by Thumbnailer and
it will be maintained in the future.
There are use cases where a simple API is enough and it is preferred over a more complex one.

Synchronous requests timeout is controlled by `Security__ProcessTimeout` setting,
as described in the [Configuration section](#configuration).

With a Thumbnailer instance running, as described in the [Install section](#install),
you can generate the thumbnail of a sample image with the following request:

```http
GET /api/v1/thumbnail?fileUri=https://via.placeholder.com/2048&widthPx=256&heightPx=256&shavePx=0&fill=true&smartCrop=false&responseType=Binary HTTP/1.1
Host: localhost:8080
```

That endpoint will generate a response containing the requested thumbnail.

Please inspect Swagger documentation of [v1 endpoints][thumbnailer-swagger-v1]
in order to find further information about exposed operations.
For example, input files can also be POSTed using a `multipart/form-data` request
or with a JSON payload.

### Asynchronous API (v2)

Asynchronous API is exposed through [v2 endpoints][thumbnailer-swagger-v2] and
it allows to start processing jobs which can be monitored with a dedicated API.
When a job is finished, its output will be available for download.

Asynchronous API is more complex to work with, because it requires at least three
HTTP calls in order to obtain the operation output. However, the asynchronous
approach is required when potentially large or complex files need to be processed.

Asynchronous jobs timeout is controlled by `Security__AsyncProcessTimeout` setting,
as described in the [Configuration section](#configuration).

With a Thumbnailer instance running, as described in the [Install section](#install),
you can start the generation of the thumbnail of a sample image with the following request:

```http
GET /api/v2/thumbnail?fileUri=https://via.placeholder.com/2048&widthPx=256&heightPx=256&shavePx=0&fill=true&smartCrop=false HTTP/1.1
Host: localhost:8080
```

That endpoint will respond with a JSON payload containing the job ID:

```json
{
    "failureReason": null,
    "jobId": "NEW_JOB_ID",
    "status": "Queued"
}
```

That job ID can be used to query the job status:

```http
GET /api/v2/jobs/NEW_JOB_ID HTTP/1.1
Host: localhost:8080
```

When job status becomes `Processed`, as in sample below:

```json
{
    "failureReason": null,
    "jobId": "NEW_JOB_ID",
    "status": "Processed"
}
```

Then job result is ready to be downloaded from following endpoint:

```http
GET /api/v2/jobs/NEW_JOB_ID/download?responseType=Binary HTTP/1.1
Host: localhost:8080
```

That endpoint will generate a response containing the requested thumbnail.

Please inspect Swagger documentation of [v2 endpoints][thumbnailer-swagger-v2]
in order to find further information about exposed operations.
For example, input files can also be POSTed using a `multipart/form-data` request
or with a JSON payload.

## Maintainers

[@pomma89][gitlab-pomma89].

## Contributing

MRs accepted.

Small note: If editing the README, please conform to the [standard-readme][github-standard-readme] specification.

### Editing

[Visual Studio Code][vscode-website], with [Remote Containers extension][vscode-remote-containers],
is the recommended way to work on this project.

A development container has been configured with all required tools.

[Visual Studio Community][vs-website] is also supported
and an updated solution file, `thumbnailer.sln`, has been provided.

### Restoring dependencies

When opening the development container, dependencies should be automatically restored.

Anyway, dependencies can be restored with following command:

```bash
dotnet restore
```

### Starting development server

A local development server listening on port `8080` can be started with following command:

```bash
dotnet run --project ./src/PommaLabs.Thumbnailer/PommaLabs.Thumbnailer.csproj
```

### Running tests

Tests can be run with following command:

```bash
export Security__AllowAnonymousAccess=true
dotnet test
```

Tests can also be run with following command, which collects coverage information:

```bash
export Security__AllowAnonymousAccess=true
./build.sh --target run-tests
```

### Building Docker image

Docker image can be built with following command:

```bash
docker build . -f ./src/PommaLabs.Thumbnailer/Dockerfile.latest -t $DOCKER_TAG
```

Please replace `$DOCKER_TAG` with a valid tag (e.g. `thumbnailer`).

## License

MIT © 2019-2022 [Alessio Parma][personal-website]

[docker-pulls-badge]: https://img.shields.io/docker/pulls/pommalabs/thumbnailer?style=flat-square
[docker-repository]: https://hub.docker.com/r/pommalabs/thumbnailer
[docker-stars-badge]: https://img.shields.io/docker/stars/pommalabs/thumbnailer?style=flat-square
[doxygen-docs]: https://pommalabs.gitlab.io/thumbnailer/
[doxygen-docs-badge]: https://img.shields.io/badge/Doxygen-OK-green?style=flat-square
[ffmpeg-website]: https://ffmpeg.org/
[github-standard-readme]: https://github.com/RichardLitt/standard-readme
[github-standard-readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[gitlab-kvlite]: https://gitlab.com/pommalabs/kvlite
[gitlab-pipeline-status-badge]: https://gitlab.com/pommalabs/thumbnailer/badges/main/pipeline.svg?style=flat-square
[gitlab-pipelines]: https://gitlab.com/pommalabs/thumbnailer/pipelines
[gitlab-pomma89]: https://gitlab.com/pomma89
[graphicsmagick-psd-disabled]: http://www.graphicsmagick.org/NEWS.html#may-30-2016
[graphicsmagick-website]: http://www.graphicsmagick.org/
[libreoffice-website]: https://www.libreoffice.org/
[nuget-downloads-badge]: https://img.shields.io/nuget/dt/PommaLabs.Thumbnailer.Client?style=flat-square
[nuget-package]: https://www.nuget.org/packages/PommaLabs.Thumbnailer.Client/
[nuget-version-badge]: https://img.shields.io/nuget/v/PommaLabs.Thumbnailer.Client?style=flat-square
[paypal-donations]: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA
[paypal-donations-badge]: https://img.shields.io/badge/Donate-PayPal-important.svg?style=flat-square
[personal-website]: https://alessioparma.xyz/
[phantomjs-website]: https://phantomjs.org/
[pngquant-website]: https://pngquant.org/
[project-license]: https://gitlab.com/pommalabs/thumbnailer/-/blob/main/LICENSE
[project-license-badge]: https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square
[renovate-badge]: https://img.shields.io/badge/renovate-enabled-brightgreen.svg?style=flat-square
[renovate-website]: https://renovate.whitesourcesoftware.com/
[sonar-coverage-badge]: https://img.shields.io/sonar/coverage/pommalabs_thumbnailer?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-quality-gate-badge]: https://img.shields.io/sonar/quality_gate/pommalabs_thumbnailer?server=https%3A%2F%2Fsonarcloud.io&sonarVersion=8&style=flat-square
[sonar-website]: https://sonarcloud.io/dashboard?id=pommalabs_thumbnailer
[thumbnailer-samples]: https://gitlab.com/pommalabs/thumbnailer/-/tree/main/tests/PommaLabs.Thumbnailer.IntegrationTests/Samples
[thumbnailer-mysql-schema]: https://gitlab.com/pommalabs/thumbnailer/-/blob/main/tests/PommaLabs.Thumbnailer.IntegrationTests/SqlScripts/mysql.sql
[thumbnailer-postgresql-schema]: https://gitlab.com/pommalabs/thumbnailer/-/blob/main/tests/PommaLabs.Thumbnailer.IntegrationTests/SqlScripts/postgresql.sql
[thumbnailer-sqlserver-schema]: https://gitlab.com/pommalabs/thumbnailer/-/blob/main/tests/PommaLabs.Thumbnailer.IntegrationTests/SqlScripts/sqlserver.sql
[thumbnailer-swagger-index]: https://thumbnailer.pommalabs.xyz/swagger/index.html
[thumbnailer-swagger-v1]: https://thumbnailer.pommalabs.xyz/swagger/index.html?urls.primaryName=V1
[thumbnailer-swagger-v2]: https://thumbnailer.pommalabs.xyz/swagger/index.html?urls.primaryName=V2
[vs-website]: https://visualstudio.microsoft.com/
[vscode-remote-containers]: https://code.visualstudio.com/docs/remote/containers
[vscode-website]: https://code.visualstudio.com/
